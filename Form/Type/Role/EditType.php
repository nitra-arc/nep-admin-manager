<?php

namespace Nitra\ManagerBundle\Form\Type\Role;

use Admingenerated\NitraManagerBundle\Form\BaseRoleType\EditType as BaseEditType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class EditType extends BaseEditType
{
    protected $options;

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->options = $options;

        parent::buildForm($builder, $options);
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'menuAccess' => array(),
        ));
    }

    /**
     * Customize options for field
     * @param string $name
     * @param array $formOptions
     * @return array
     */
    protected function getFormOption($name, array $formOptions)
    {
        switch ($name) {
            case 'menuAccess':
                $formOptions['choices'] = $this->options['menuAccess'];
                break;
        }

        return $formOptions;
    }
}