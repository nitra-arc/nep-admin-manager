# ManagerBundle

## Описание

данный бандл предназначен для:

* **управления учетными записями менеджеров и администраторов**

## Подключение

* **composer.json**

```json
{
    ...   
    "require": {
        ...
        "nitra/e-commerce-admin-managerbundle": "dev-master",
        ...
    }
    ...
}
```

* **app/AppKernel.php**

```php
<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    //...
    public function registerBundles()
    {
        //...
        $bundles = array(
            //...
            new Nitra\ManagerBundle\NitraManagerBundle(),
            //...
        );
        //...
        return $bundles;
    }
    //...
}
```

## Конфигурация

```yaml
# app/config/config.yml
nitra_manager:
    access_firewall: false|true
```

* access_firewall - Использовать ли привелегии пользователей при генерации меню и проверять на наличие привелегии при переходи по ссылке