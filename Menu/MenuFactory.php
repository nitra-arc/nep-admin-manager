<?php

namespace Nitra\ManagerBundle\Menu;

use Millwright\MenuBundle\Menu\MenuFactory as BaseMenuFactory;
use Knp\Menu\ItemInterface as MenuItemInterface;

class MenuFactory extends BaseMenuFactory
{
    /**
     * Check menu item is allowed by menu access
     * 
     * @param MenuItemInterface $item
     * 
     * @return boolean
     */
    protected function isAllowedByMenuAccess($item)
    {
        foreach ($this->security->getToken()->getUser()->getStatus() as $role) {
            foreach ($role->getMenuAccess() as $menuAccess) {
                if ($menuAccess == $item->getName()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    protected function setContext(MenuItemInterface $item, array $routeParameters = array(), array $options = array())
    {
        parent::setContext($item, $routeParameters, $options);

        if (!$item->isDisplayed()) {
            return $this;
        }

        if (!$this->isAllowedByMenuAccess($item)) {
            $item->setDisplay(false);
        }

        return $this;
    }
}