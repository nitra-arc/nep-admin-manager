<?php

namespace Nitra\ManagerBundle\Listener;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\Security\Core\SecurityContext;

/**
 * Custom login listener.
 */
class LoginListener
{
    /** @var SecurityContext */
    protected $context;

    /**
     * Constructor
     *
     * @param SecurityContext $context
     */
    public function __construct(SecurityContext $context)
    {
        $this->context = $context;
    }

    /**
     * Add user store id to session.
     *
     * @param Event $event
     */
    public function onSecurityInteractiveLogin(Event $event)
    {
        $stores = $this->context->getToken()->getUser()->getStores();

        $session = $event->getRequest()->getSession();

        if ($stores) {
            foreach ($stores as $store) {
                $session->set('store_id', $store->getId());
            }
        } else {
            $session->set('store_id', 0);
        }
    }
}