<?php

namespace Nitra\ManagerBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Security\Core\Role\RoleInterface;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ODM\Document
 */
class Role implements RoleInterface
{
    use \Gedmo\Blameable\Traits\BlameableDocument;
    use \Gedmo\Timestampable\Traits\TimestampableDocument;

    /**
     * @ODM\Id
     */
    protected $id;

    /**
     * @var string name
     * @ODM\String
     */
    protected $name;

    /**
     * @var string role
     * @ODM\String
     */
    protected $role;

    /**
     * @var array menuAccess
     * @ODM\Hash
     */
    protected $menuAccess = array();

    public function __toString()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     * @param string $name
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set role
     * @param string $role
     * @return self
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Get menuAccess
     * @return array
     */
    public function getMenuAccess()
    {
        return $this->menuAccess;
    }

    /**
     * Set menuAccess
     * @param array $menuAccess
     * @return self
     */
    public function setMenuAccess(array $menuAccess)
    {
        $this->menuAccess = $menuAccess;

        return $this;
    }
}