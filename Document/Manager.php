<?php

namespace Nitra\ManagerBundle\Document;

use FOS\UserBundle\Document\User as BaseUser;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ODM\Document
 */
class Manager extends BaseUser
{
    /**
     * @ODM\Id
     */
    protected $id;

    /**
     * @var string $phone
     * @ODM\String
     * @Assert\Length(max = 15)
     */
    protected $phone;

    /**
     * @var string $firstname
     * @ODM\String
     * @Assert\Length(max = 255)
     */
    protected $firstname;

    /**
     * @var string $lastname
     * @ODM\String
     * @Assert\Length(max = 255)
     */
    protected $lastname;

    /**
     * @ODM\ReferenceMany(targetDocument="Nitra\StoreBundle\Document\Store")
     */
    protected $stores;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     * @ODM\ReferenceMany(targetDocument="Nitra\ManagerBundle\Document\Role")
     */
    protected $status;

    public function __construct()
    {
        parent::__construct();

        $this->stores = new \Doctrine\Common\Collections\ArrayCollection();
        $this->status = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set phone
     *
     * @param string $phone
     * @return Manager
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return Manager
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return Manager
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Add stores
     *
     * @param Nitra\StoreBundle\Document\Store $stores
     */
    public function addStores(\Nitra\StoreBundle\Document\Store $stores)
    {
        $this->stores[] = $stores;
    }

    /**
     * Set stores
     *
     * @param  $stores
     */
    public function setStores($stores)
    {
        $this->stores = $stores;
    }

    /**
     * Get stores
     *
     * @return Doctrine\Common\Collections\Collection $stores
     */
    public function getStores()
    {
        return $this->stores;
    }

    /**
     * Add status
     *
     * @param \Nitra\ManagerBundle\Document\Role $status
     */
    public function addStatus($status)
    {
        $this->status[] = $status;
    }

    /**
     * Get roles
     * @return Doctrine\Common\Collections\Collection $roles
     */
    public function getRoles()
    {
        $roles = array();
        foreach ($this->status as $r) {
            $roles[] = $r->getRole();
        }

        return $roles;
    }

    public function getFOSRoles()
    {
        $roles = $this->roles;

        foreach ($this->getGroups() as $group) {
            $roles = array_merge($roles, $group->getRoles());
        }

        // we need to make sure to have at least one role
        $roles[] = static::ROLE_DEFAULT;

        return array_unique($roles);
    }

    /**
     * Get status
     * @return Role[] $status
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function getRolesText()
    {
        return implode(', ', $this->status->toArray());
    }

    public function removeStatus($status)
    {
        $this->status->removeElement($status);
        return $this;
    }
}