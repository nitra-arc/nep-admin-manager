<?php

namespace Nitra\ManagerBundle\Routing;

use Symfony\Bundle\FrameworkBundle\Routing\Router as BaseRouter;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class Router extends BaseRouter
{
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected $container;

    /**
     * {@inheritDoc}
     */
    public function __construct(ContainerInterface $container, $resource, array $options = array(), RequestContext $context = null)
    {
        $this->container = $container;

        parent::__construct($container, $resource, $options, $context);
    }

    /**
     * {@inheritdoc}
     */
    public function match($pathinfo)
    {
        $parameters = $this->getMatcher()->match($pathinfo);

        $this->checkAccessToRouteByMenu();

        return $parameters;
    }

    protected function checkAccessToRouteByMenu()
    {
        if (!$this->container->isScopeActive('request')) {
            return true;
        }

        $request = $this->container->get('request');
        if (!($activeMenuItem = $this->getActiveMenuItemName($request->getRequestUri()))) {
            return;
        }

        $token = $this->container->get('security.context')->getToken();
        if (!$token || !$token->isAuthenticated()) {
            return;
        }
        if (!$this->isAllowedByMenuAccess($token, $activeMenuItem)) {
            throw $this->createAccessDeniedException();
        }
    }

    /**
     * Find active menu item by request uri
     * 
     * @param string $requestUri
     * 
     * @return boolean|string
     */
    protected function getActiveMenuItemName($requestUri)
    {
        $items = $this->container->getParameter('millwright_menu.items');

        foreach ($items as $name => $item) {
            if (!key_exists('route', $item)) {
                continue;
            }
            $route = $this->generate($item['route']);
            $pattern = '/' . preg_quote($route, '/') . '.*/';

            if (preg_match($pattern, $requestUri)) {
                return $name;
            }
        }

        return false;
    }

    /**
     * Check menu item is allowed by menu access
     * 
     * @param \Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token
     * @param string $menuItemName
     * 
     * @return boolean
     */
    protected function isAllowedByMenuAccess($token, $menuItemName)
    {
        foreach ($token->getUser()->getStatus() as $role) {
            foreach ($role->getMenuAccess() as $menuAccess) {
                if ($menuAccess == $menuItemName) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Create exception
     * 
     * @return \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    protected function createAccessDeniedException()
    {
        return new AccessDeniedException();
    }
}