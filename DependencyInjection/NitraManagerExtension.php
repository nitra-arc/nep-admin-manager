<?php

namespace Nitra\ManagerBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\Config\FileLocator;

class NitraManagerExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        if ($config['access_firewall']) {
            $container->setParameter('millwright_menu.factory.class', 'Nitra\ManagerBundle\Menu\MenuFactory');
            $container->setParameter('router.class', 'Nitra\ManagerBundle\Routing\Router');
        }
    }
}