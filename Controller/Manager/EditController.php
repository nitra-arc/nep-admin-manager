<?php

namespace Nitra\ManagerBundle\Controller\Manager;

use Admingenerated\NitraManagerBundle\BaseManagerController\EditController as BaseEditController;

class EditController extends BaseEditController
{
    /**
     * This method is here to make your life better, so overwrite  it
     *
     * @param \Symfony\Component\Form\Form $form the valid form
     * @param \Nitra\ManagerBundle\Document\Manager $Manager
     */
    public function preSave(\Symfony\Component\Form\Form $form, \Nitra\ManagerBundle\Document\Manager $Manager)
    {
        if ($form->getData()->getPassword()) {
            $Manager->setPlainPassword($form->getData()->getPassword());
        } else {
            $oldPassword = $this->getDocumentManager()->getUnitOfWork()->getOriginalDocumentData($Manager)['password'];
            $Manager->setPassword($oldPassword);
        }
    }
}