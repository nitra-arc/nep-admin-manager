<?php

namespace Nitra\ManagerBundle\Controller\Manager;

use Admingenerated\NitraManagerBundle\BaseManagerController\NewController as BaseNewController;

class NewController extends BaseNewController
{
    public function preSave(\Symfony\Component\Form\Form $form, \Nitra\ManagerBundle\Document\Manager $Manager)
    {
        if ($form->getData()->getPassword()) {
            $Manager->setPlainPassword($form->getData()->getPassword());
        }
    }
}