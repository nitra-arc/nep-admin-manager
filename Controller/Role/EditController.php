<?php

namespace Nitra\ManagerBundle\Controller\Role;

use Admingenerated\NitraManagerBundle\BaseRoleController\EditController as BaseEditController;

class EditController extends BaseEditController
{
    /**
     * Creates and returns a Form instance from the type of the form.
     *
     * @param string|FormTypeInterface $type    The built type of the form
     * @param mixed                    $data    The initial data for the form
     * @param array                    $options Options for the form
     *
     * @return Form
     */
    public function createForm($type, $data = null, array $options = array())
    {
        $items = array_keys($this->container->getParameter('millwright_menu.items'));

        $options['menuAccess'] = $this->transMenuTree($items);

        return parent::createForm($type, $data, $options);
    }

    /**
     * Trans items
     * @param array $items
     * @return array
     */
    protected function transMenuTree($items)
    {
        $result = array();
        foreach ($items as $item) {
            $result[$item] = $this->get('translator')->trans($item, array(), 'menu');
        }

        return $result;
    }
}