<?php

namespace Nitra\ManagerBundle\Command; 

use Nitra\ExtensionsBundle\Command\NitraContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Nitra\ManagerBundle\Document\Role;

class AddRoleUserCommand extends NitraContainerAwareCommand
{
    /**
     * @see Command
     */
    protected function configure()
    {
        $this
            ->setName('nitra:manager:add-role-user')
            ->setDescription('Create role user.');
    }

    /**
     * для создания ролей и добавления связей к пользователю
     * @see Command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dm = $this->getDocumentManager();

        $roles = $this->getRoles();

        $users = $dm->getRepository('NitraManagerBundle:Manager')->findAll();

        $progress = $this->getProgressHelper();
        $progress->start($output, $users->count());
        foreach ($users as $user) {
            foreach ($user->getFOSRoles() as $userRole) {
                if (key_exists($userRole, $roles)) {
                    $user->addStatus($roles[$userRole]);
                }
            }
            $user->setRoles(array());
            $progress->advance();
        }
        $progress->finish();
        foreach ($roles as $role) {
            $dm->persist($role);
        }
        $dm->flush();

        $output->writeln('ok');
    }

    /**
     * @return Role[]
     */
    protected function getRoles()
    {
        $roles = array(
            'ROLE_SUPER_ADMIN'  => new Role(),
            'ROLE_ADMIN'        => new Role(),
            'ROLE_USER'         => new Role(),
        );

        $roles['ROLE_SUPER_ADMIN']
            ->setName('Супер администратор')
            ->setRole('ROLE_SUPER_ADMIN');

        $roles['ROLE_ADMIN']
            ->setName('Администратор')
            ->setRole('ROLE_ADMIN');

        $roles['ROLE_USER']
            ->setName('Пользователь')
            ->setRole('ROLE_USER');

        return $roles;
    }
}